import React, {Component} from 'react';
import {createStore, applyMiddleware} from 'redux';
import reduxThunk from 'redux-thunk';
import {Provider} from 'react-redux';
import firebase from 'firebase';

import Router from './Router';
import reducers from './reducers';

class App extends Component {
    componentWillMount() {
        const config = {
            apiKey: 'AIzaSyCimT6ELtYlVG53m_BLjbMd2wyGPJ_ky5Y',
            authDomain: 'manager-b2183.firebaseapp.com',
            databaseURL: 'https://manager-b2183.firebaseio.com',
            projectId: 'manager-b2183',
            storageBucket: 'manager-b2183.appspot.com',
            messagingSenderId: '354905511695'
        };
        firebase.initializeApp(config);
    }

    render() {
        return (
            <Provider store={createStore(reducers, {}, applyMiddleware(reduxThunk))}>
                <Router />
            </Provider>
        );
    }
}

export default App;