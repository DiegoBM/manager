import {Actions} from 'react-native-router-flux';
import firebase from 'firebase';

import {EMAIL_CHANGED, PASSWORD_CHANGED, LOGIN_USER_SUCCESS, LOGIN_USER_FAIL, LOGIN_USER} from './types';

export const emailChanged = email => ({type: EMAIL_CHANGED, payload: email});
export const passwordChanged = password => ({type: PASSWORD_CHANGED, payload: password});

const loginUserSuccess = dispatch => user => {
    dispatch({type: LOGIN_USER_SUCCESS, payload: user});
    Actions.main();
};

const loginUserFail = dispatch => ({message}) => {
    dispatch({type: LOGIN_USER_FAIL, payload: message});
};

export const loginUser = ({email, password}) => {
    return dispatch => {
        dispatch({type: LOGIN_USER});
        firebase.auth().signInWithEmailAndPassword(email, password)
            .then(loginUserSuccess(dispatch))
            .catch(() => {
                firebase.auth().createUserWithEmailAndPassword(email,  password)
                    .then(loginUserSuccess(dispatch))
                    .catch(loginUserFail(dispatch));
            });
    }
}