import {Actions} from 'react-native-router-flux';
import firebase from 'firebase';

import {EMPLOYEE_UPDATE, EMPLOYEE_CREATE, EMPLOYEES_FETCH_SUCCESS, EMPLOYEE_SAVE_SUCCESS} from './types';

export const employeeUpdate = ({prop, value}) => ({type: EMPLOYEE_UPDATE, payload: {prop, value}});
export const employeeCreate = ({name, phone, shift}) => {
    const userId = firebase.auth().currentUser.uid;
    return dispatch => {
        firebase.database().ref(`/users/${userId}/employees`).push({name, phone, shift})
            .then(() => {
                dispatch({type: EMPLOYEE_CREATE});
                Actions.employeeList({type: 'reset'});
            });
    };
};
export const employeesFetch = () => {
    const userId = firebase.auth().currentUser.uid;
    return dispatch => {
        firebase.database().ref(`/users/${userId}/employees`).on('value', snapshot => {
            dispatch({type: EMPLOYEES_FETCH_SUCCESS, payload: snapshot.val()});
        });
    }

};
export const employeeSave = ({name, phone, shift, uid}) => {
    const userId = firebase.auth().currentUser.uid;
    return dispatch => {
        firebase.database().ref(`/users/${userId}/employees/${uid}`).set({name, phone, shift})
            .then(() => {
                dispatch({type: EMPLOYEE_SAVE_SUCCESS});
                Actions.employeeList({type: 'reset'});
            });
    };
};

export const employeeDelete = ({uid}) => {
    const userId = firebase.auth().currentUser.uid;
    return () => {
        firebase.database().ref(`/users/${userId}/employees/${uid}`).remove()
            .then(() => Actions.employeeList({type: 'reset'}));
    };
};