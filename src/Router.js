import React from 'react';
import {StyleSheet, Platform} from 'react-native';
import {Scene, Router, Actions} from 'react-native-router-flux';

import LoginForm from './components/LoginForm';
import EmployeeList from './components/EmployeeList';
import EmployeeCreate from './components/EmployeeCreate';
import EmployeeEdit from './components/EmployeeEdit';

const RouterComponent = _ => (
    <Router sceneStyle={styles.Router}>
        <Scene key="auth">
            <Scene key="login" component={LoginForm} title="Please Login"/>
        </Scene>
        <Scene key="main">
            <Scene key="employeeList"
                   component={EmployeeList}
                   title="Employees"
                   rightTitle="Add"
                   onRight={_ => Actions.employeeCreate()}
                   initial/>
            <Scene key="employeeCreate"
                   component={EmployeeCreate}
                   title="Create Employee"/>
            <Scene key="employeeEdit"
                   component={EmployeeEdit}
                   title="Edit Employee"/>
        </Scene>
    </Router>
);

const isAndroid = (Platform.OS === 'android');
const styles = StyleSheet.create({
    Router: {
        paddingTop: isAndroid ? 55 : 65
    }
});

export default RouterComponent;