import React, {Component} from 'react';
import {Text, StyleSheet} from 'react-native';
import {connect} from 'react-redux';

import {Card, CardSection, Input, Button, Spinner} from './common';
import {emailChanged, passwordChanged, loginUser} from '../actions';

class LoginForm extends Component {

    handleEmailChange = email => {
        this.props.emailChanged(email);
    };

    handlePasswordChange = password => {
        this.props.passwordChanged(password);
    };

    handleButtonPress = () => {
        const {email, password, loginUser} = this.props;
        loginUser({email, password});
    };

    renderButton(loading) {
        if (loading) {
            return <Spinner size="small"/>
        } else {
            return <Button onPress={this.handleButtonPress}>Log in</Button>
        }
    };

    render() {
        const {email, password, loading, error} = this.props;
        return (
            <Card>
                <CardSection>
                    <Input
                        value={email}
                        label="Email"
                        placeholder="user@email.com"
                        onChangeText={this.handleEmailChange}/>
                </CardSection>
                <CardSection>
                    <Input
                        value={password}
                        secureTextEntry
                        label="Password"
                        placeholder="password"
                        onChangeText={this.handlePasswordChange}/>
                </CardSection>
                <Text style={styles.error}>{error}</Text>
                <CardSection>
                    {this.renderButton(loading)}
                </CardSection>
            </Card>
        );
    }
}

const styles = StyleSheet.create({
    error: {
        fontSize: 20,
        alignSelf: 'center',
        color: 'red'
    }
});

const mapStateToProps = ({auth: {email, password, loading, error}}) => ({email, password, loading, error});
export default connect(mapStateToProps, {emailChanged, passwordChanged, loginUser})(LoginForm);