import React from 'react';
import {Text, View, StyleSheet} from 'react-native';

const Header = ({caption = 'Albums!'}) => (
    <View style={styles.viewStyle}>
        <Text style={styles.textStyle}>{caption}</Text>
    </View>
);

const styles = StyleSheet.create({
    viewStyle: {
        backgroundColor: '#f8f8f8',
        height: 60,
        paddingTop: 15,
        alignItems: 'center',
        justifyContent: 'center',
        // iOS shadow (not supported in Android)
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2},
        shadowOpacity: 0.2,
        // Android elevation
        elevation: 2,
        position: 'relative'
    },
    textStyle: {
        fontSize: 20
    }
});

export {Header};