import React from 'react';
import {View, StyleSheet} from 'react-native';

const CardSection = ({children, style}) => (
    <View style={[styles.CardSection, style]}>{children}</View>
);

const styles = StyleSheet.create({
    CardSection: {
        borderBottomWidth: 1,
        borderColor: '#ddd',
        padding: 5,
        backgroundColor: '#fff',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        position: 'relative'
    }
});

export {CardSection};