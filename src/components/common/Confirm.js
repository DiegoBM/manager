import React from 'react';
import {View, Text, Modal, StyleSheet} from 'react-native';

// Don't import from index.js to avoid cyclic imports
import {CardSection} from './CardSection';
import {Button} from './Button';

const Confirm = ({children, visible, handleAccept, handleDecline}) => (
    <Modal visible={visible}
           transparent
           animationType="slide"
           onRequestClose={() => {}}>
        <View style={styles.Confirm}>
            <CardSection style={styles.card}>
                <Text style={styles.text}>{children}</Text>
            </CardSection>
            <CardSection>
                <Button onPress={handleAccept}>Yes</Button>
                <Button onPress={handleDecline}>No</Button>
            </CardSection>
        </View>
    </Modal>
);

const styles = StyleSheet.create({
    Confirm: {backgroundColor: 'rgba(0, 0, 0, 0.75)', position: 'relative', flex: 1, justifyContent: 'center'},
    card: {justifyContent: 'center'},
    text: {flex: 1, fontSize: 18, textAlign: 'center', lineHeight: 40}
});

export {Confirm};