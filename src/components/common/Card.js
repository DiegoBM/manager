import React from 'react';
import {View, StyleSheet} from 'react-native';

const Card = ({children}) => (
    <View style={styles.Card}>
        {children}
    </View>
);

const styles = StyleSheet.create({
    Card: {
        // Border
        borderWidth: 1,
        borderRadius: 2,
        borderColor: '#ddd',
        borderBottomWidth: 0,
        // Margin
        marginLeft: 5,
        marginRight: 5,
        marginTop: 10,
        // Shadow iOS
        shadowColor: '#000',
        shadowOffset: {width: 0, height: 2},
        shadowOpacity: 0.1,
        shadowRadius: 2,
        // shadow Android
        elevation: 1
    }
});

export {Card};