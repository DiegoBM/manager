import React, {Component} from 'react';
import {View, Text, Picker, StyleSheet} from 'react-native'
import {connect} from 'react-redux';

import {employeeUpdate, employeeCreate} from '../actions';
import {CardSection, Input} from './common';

class EmployeeForm extends Component {
    render() {
        const {name, phone, shift, employeeUpdate} = this.props;
        const shifts = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];

        return (
            <View>
                <CardSection>
                    <Input label="Name"
                           placeholder="Jane"
                           value={name}
                           onChangeText={value => employeeUpdate({prop: 'name', value})}/>
                </CardSection>
                <CardSection>
                    <Input label="Phone"
                           placeholder="555-555-5555"
                           value={phone}
                           onChangeText={value => employeeUpdate({prop: 'phone', value})}/>
                </CardSection>
                <CardSection style={styles.container}>
                    <Text style={styles.label}>Shift</Text>
                    <Picker selectedValue={shift}
                            onValueChange={value => employeeUpdate({prop: 'shift', value})}
                    >
                        {shifts.map(shift => <Picker.Item key={shift} label={shift} value={shift} />)}
                    </Picker>
                </CardSection>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {flexDirection: 'column'},
    label: {fontSize: 18, paddingLeft: 20},
    picker: {flex: 1}
});

export const mapStateToProps = ({employee: {name, phone, shift}}) => ({name, phone, shift});
export default connect(mapStateToProps, {employeeUpdate, employeeCreate})(EmployeeForm);