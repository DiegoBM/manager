import React, {Component} from 'react';
import {connect} from 'react-redux';

import {Card, CardSection, Button} from './common';
import {employeeCreate} from '../actions';
import EmployeeForm from './EmployeeForm';

class EmployeeCreate extends Component {
    handleButtonPress = () => {
        const {name, phone, shift, employeeCreate} = this.props;
        employeeCreate({name, phone, shift: shift || 'Monday'});
    };

    render() {
        return (
            <Card>
                <EmployeeForm {...this.props} />
                <CardSection>
                    <Button onPress={this.handleButtonPress}>Create</Button>
                </CardSection>
            </Card>
        );
    }
}

export const mapStateToProps = ({employee: {name, phone, shift}}) => ({name, phone, shift});
export default connect(mapStateToProps, {employeeCreate})(EmployeeCreate);