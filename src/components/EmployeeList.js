import React, {Component} from 'react';
import {ListView, Text} from 'react-native';
import {connect} from 'react-redux';
import _ from 'lodash';

import {employeesFetch} from '../actions';
import ListItem from './ListItem';

class EmployeeList extends Component {
    componentWillMount() {
        this.props.employeesFetch();

        this.ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.dataSource = this.ds.cloneWithRows(this.props.employees);
    }

    componentWillReceiveProps(nextProps) {
        this.dataSource = this.ds.cloneWithRows(nextProps.employees);
    }

    renderEmployee(employee) {
        return <ListItem employee={employee} />
    }

    render() {
        return (
            <ListView enableEmptySections
                      dataSource={this.dataSource}
                      renderRow={this.renderEmployee} />
        );
    }
}

const mapStateToProps = ({employees}) => ({employees: _.map(employees, (val, uid) => ({...val, uid}))});
export default connect(mapStateToProps, {employeesFetch})(EmployeeList);