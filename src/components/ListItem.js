import React, {Component} from 'react';
import {Text, View, TouchableWithoutFeedback, StyleSheet} from 'react-native';
import {Actions} from 'react-native-router-flux';

import {CardSection} from './common';

class ListItem extends Component {
    handleRowPress = () => {
        Actions.employeeEdit({employee: this.props.employee});
    };

    render() {
        return (
            <TouchableWithoutFeedback onPress={this.handleRowPress}>
                <View>
                    <CardSection>
                        <Text style={styles.title}>{this.props.employee.name}</Text>
                    </CardSection>
                </View>
            </TouchableWithoutFeedback>
        );
    }
}

const styles = StyleSheet.create({
    title: {
        fontSize: 18,
        paddingLeft: 15
    }
});

export default ListItem;