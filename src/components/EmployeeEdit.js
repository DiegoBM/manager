import _ from 'lodash';
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {text} from 'react-native-communications';

import {Card, CardSection, Button, Confirm} from './common';
import {employeeUpdate, employeeSave, employeeDelete} from '../actions';
import EmployeeForm from './EmployeeForm';

class EmployeeEdit extends Component {
    state = {
        showModal: false
    };

    componentWillMount() {
        _.each(this.props.employee, (value, prop) => this.props.employeeUpdate({prop, value}));
    }

    handleSaveButtonPress = () => {
        const {name, phone, shift, employeeSave, employee: {uid}} = this.props;
        employeeSave({name, phone, shift, uid});
    };

    handleTextButtonPress = () => {
        const {phone, shift} = this.props;
        text(phone, `Your upcomming shift is on ${shift}`);
    };

    handleDeleteButtonPress = () => {
        this.setState({showModal: true});
    };

    handleDeleteAccept = () => {
        this.setState({showModal: false});
        this.props.employeeDelete({uid: this.props.employee.uid});
    };

    handleDeleteDecline = () => {
        this.setState({showModal: false});
    };

    render() {
        return (
            <Card>
                <EmployeeForm {...this.props} />
                <CardSection>
                    <Button onPress={this.handleSaveButtonPress}>Save Changes</Button>
                </CardSection>
                <CardSection>
                    <Button onPress={this.handleTextButtonPress}>Text Schedule</Button>
                </CardSection>
                <CardSection>
                    <Button onPress={this.handleDeleteButtonPress}>Fire Employee</Button>
                </CardSection>
                <Confirm visible={this.state.showModal}
                         handleAccept={this.handleDeleteAccept}
                         handleDecline={this.handleDeleteDecline}>
                    Are you sure you want to delete this?
                </Confirm>
            </Card>
        );
    }
}

export const mapStateToProps = ({employee: {name, phone, shift}}) => ({name, phone, shift});
export default connect(mapStateToProps, {employeeUpdate, employeeSave, employeeDelete})(EmployeeEdit);