import {combineReducers} from 'redux';

import auth from './auth';
import employee from './employee';
import employees from './employees';

export default combineReducers({auth, employee, employees});